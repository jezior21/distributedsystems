﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab01
{
    class BestFit : FastFit
    {
        public BestFit(List<MemoryItem> memoryItems, int memorySize, int maxChunkSize, int ocupationMaxTime,
            int simulationTime, Random stepRand, Random sizeRand, int intervalMaxStep, Random ocupationRand)
            : base(
                memoryItems, memorySize, maxChunkSize, ocupationMaxTime, simulationTime, stepRand, sizeRand,
                intervalMaxStep, ocupationRand)
        {
        }

        public override void PrintResult()
        {
            Console.WriteLine("BestFit for simulation {0} rejected={1}", SimulationTime, Rejected);

        }

        public override void ChooseAndFill(int timeStamp)
        {
            var size = SizeRand.Next(MaxChunkSize);
            var availableSizes = Utils.AvailableSizes(MemoryItems, size, timeStamp);

            if (!availableSizes.Any())
            {
                Rejected++;
                return;
            }

            var minimum = availableSizes.Min(available => available.Item2 - size);
            var choosen = availableSizes.First(available => (available.Item2 - size) == minimum);
            Utils.Fill(MemoryItems, size, choosen.Item1, timeStamp + OcupationRand.Next(OcupationMaxTime));

        }
    }
}
