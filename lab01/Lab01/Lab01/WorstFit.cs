﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Lab01
{
    public class WorstFit : FastFit
    {
        public WorstFit(List<MemoryItem> memoryItems, int memorySize, int maxChunkSize, int ocupationMaxTime,
            int simulationTime, Random stepRand, Random sizeRand, int intervalMaxStep, Random ocupationRand)
            : base(
                memoryItems, memorySize, maxChunkSize, ocupationMaxTime, simulationTime, stepRand, sizeRand,
                intervalMaxStep, ocupationRand)
        {
        }

        public override void PrintResult()
        {
            Console.WriteLine("WorstFit for simulation {0} rejected={1}", SimulationTime, Rejected);

        }

        public override void ChooseAndFill(int timeStamp)
        {
            var size = SizeRand.Next(MaxChunkSize);
            var availableSizes = Utils.AvailableSizes(MemoryItems, size, timeStamp);

            if (!availableSizes.Any())
            {
                Rejected++;
                return;
            }

            var maxAvailable = availableSizes.Max(available => available.Item2);
            var choosen = availableSizes.First(available => (available.Item2 == maxAvailable));
            Utils.Fill(MemoryItems, size, choosen.Item1, timeStamp + OcupationRand.Next(OcupationMaxTime));

        }
    }
}