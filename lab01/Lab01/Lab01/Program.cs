﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Lab01
{
    public class Program
    {
        private const int MemorySize = 256;
        private const int MaxChunkSize = 8;
        private const int StepInitSize = 32;
        private const int OcupationMaxTime = 16;
        private const int SimulationTime = 5000;
        private const int IntervalTime = 2;

        public static void Main(string[] args)
        {
            var seed = DateTime.Now.Millisecond;

            RunAndPrepareFast(seed);
            RunAndPrepareBest(seed);
            RunAndPrepareWorst(seed);
            Console.ReadKey();
        }

        public static void RunAndPrepareFast(int seed)
        {
            var memoryItems = new List<MemoryItem>();
            InitMemory(memoryItems);
            InitRandomSectors(memoryItems, seed);
            var fastFit = new FastFit(memoryItems, MemorySize, MaxChunkSize, OcupationMaxTime, SimulationTime,
    new Random(seed + 2), new Random(seed + 4), IntervalTime, new Random(seed + 8));
            fastFit.Run();
        }

        public static void RunAndPrepareBest(int seed)
        {
            var memoryItems = new List<MemoryItem>();
            InitMemory(memoryItems);
            InitRandomSectors(memoryItems, seed);
            var bestFit = new BestFit(memoryItems, MemorySize, MaxChunkSize, OcupationMaxTime, SimulationTime,
                new Random(seed + 2), new Random(seed + 4), IntervalTime, new Random(seed + 8));
            bestFit.Run();
        }

        public static void RunAndPrepareWorst(int seed)
        {
            var memoryItems = new List<MemoryItem>();
            InitMemory(memoryItems);
            InitRandomSectors(memoryItems, seed);
            var bestFit = new WorstFit(memoryItems, MemorySize, MaxChunkSize, OcupationMaxTime, SimulationTime,
                new Random(seed + 2), new Random(seed + 4), IntervalTime, new Random(seed + 8));
            bestFit.Run();
        }


        private static void PrintMemoryStatus(List<MemoryItem> memoryItems, int timeStamp)
        {
            var available = memoryItems.Count(item => item.OcupiedTill <= timeStamp);
            var ocupied = memoryItems.Count(item => item.OcupiedTill > timeStamp);
            Console.WriteLine("Ocupied: {0}, Available: {1}", ocupied, available);
        }

        private static void InitMemory(List<MemoryItem> memoryItems)
        {
            for (int i = 0; i < MemorySize; i++)
            {
               memoryItems.Add(new MemoryItem());
            }
        }

        private static void PrintMemory(List<MemoryItem> memoryItems)
        {
            memoryItems.ForEach(item => Console.WriteLine(item.OcupiedTill));
        }

        private static void InitRandomSectors(List<MemoryItem> memoryItems, int seed)
        {
            
            var rand = new Random(seed);
            int position = 0;
            while (position < MemorySize)
            {
                var next = rand.Next(MaxChunkSize, StepInitSize) + position;

                if (next >= MemorySize)
                {
                    break;
                }

                var newChunkSize = rand.Next(MaxChunkSize);
                var newTime = rand.Next(OcupationMaxTime);

                for (var i = next; i < next + newChunkSize && i < MemorySize; ++i)
                {
                    memoryItems[i].OcupiedTill += newTime;
                }
                position = next;

            }
        }
    }
}