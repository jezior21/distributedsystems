﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Lab01
{
    public class FastFit
    {
        protected readonly List<MemoryItem> MemoryItems;
        protected readonly int MemorySize;
        protected readonly int MaxChunkSize;
        protected readonly int OcupationMaxTime;
        protected readonly int SimulationTime;
        protected readonly Random StepRand;
        protected readonly Random SizeRand;
        protected readonly int IntervalMaxStep;
        protected readonly Random OcupationRand;

        protected int Rejected;

        public FastFit(List<MemoryItem> memoryItems, int memorySize, int maxChunkSize, int ocupationMaxTime, int simulationTime, 
            Random stepRand, Random sizeRand, int intervalMaxStep, Random ocupationRand)
        {
            MemoryItems = memoryItems;
            MemorySize = memorySize;
            MaxChunkSize = maxChunkSize;
            OcupationMaxTime = ocupationMaxTime;
            SimulationTime = simulationTime;
            StepRand = stepRand;
            SizeRand = sizeRand;
            IntervalMaxStep = intervalMaxStep;
            OcupationRand = ocupationRand;
        }

        public void Run()
        {
            for (var i = 0; i < SimulationTime; i += StepRand.Next(IntervalMaxStep))
            {
                ChooseAndFill(i);
            }
            PrintResult();
        }

        public virtual void PrintResult()
        {
            Console.WriteLine("FastFit for simulation {0} rejected={1}", SimulationTime, Rejected);

        }

        public virtual void ChooseAndFill(int timeStamp)
        {
            var size = SizeRand.Next(MaxChunkSize);
            var availableSizes = Utils.AvailableSizes(MemoryItems, size, timeStamp);

            if (!availableSizes.Any())
            {
                Rejected++;
                return;
            }

            var choosen = availableSizes.First();

            Utils.Fill(MemoryItems, size, choosen.Item1, timeStamp + OcupationRand.Next(OcupationMaxTime));

        }
    }
}
