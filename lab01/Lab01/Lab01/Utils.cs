﻿using System;
using System.Collections.Generic;

namespace Lab01
{
    public static class Utils
    {
        public static List<Tuple<int, int>> AvailableSizes(List<MemoryItem> memoryItems, int requestedSize, int timeStamp)
        {
            var result = new List<Tuple<int, int>>();

            var start = 0;
            var size = 0;
            var isStarted = false;

            foreach (var item in memoryItems)
            {
                if (isStarted && item.OcupiedTill <= timeStamp)
                {
                    ++size;
                    continue;
                }

                if (isStarted && item.OcupiedTill > timeStamp)
                {
                    isStarted = false;
                    if (size >= requestedSize)
                    {
                        result.Add(new Tuple<int, int>(start, size));
                    }
                    continue;
                }

                if (!isStarted && item.OcupiedTill <= timeStamp)
                {
                    isStarted = true;
                    size = 1;
                    start = memoryItems.IndexOf(item);
                }
            }

            return result;
        }

        public static void PrintAvailableSizes(List<Tuple<int, int>> availableSizes)
        {
            foreach (var availableSize in availableSizes)
            {
                Console.WriteLine("Index: {0}, Size: {1}", availableSize.Item1, availableSize.Item2);
            }
        }

        public static void Fill(List<MemoryItem> memoryItems, int requestedSize, int index, int timeStamp)
        {
            for (int i = index; i < index + requestedSize; i++)
            {
                memoryItems[i].OcupiedTill = timeStamp;
            }
        }
    }
}