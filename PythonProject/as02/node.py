from as02.request import Request


class Node:
    def __init__(self, code):
        self.code = code
        self.connections = {}
        self.responses = set()
        self.received_requests = []
        self.pending_request = None
        self.clock = 0  # indicates whether Node is in Critical Section

    def add_connections(self, nodes):
        self.connections = nodes

    def put_response(self, node_key):
        self.responses.add(node_key)

    def put_request(self, request):
        self.received_requests.append(request)

    def send_request(self, node_id, timestamp):
        self.pending_request = Request(self.code, (node_id, timestamp), 2)
        for node in self.connections.values():
            node.put_request(self.pending_request)

    def do_job(self):
        if self.clock > 0:  # node is in critical section, no job is done
            print("Node " + self.code + " is in critical section")
            self.clock -= 1
            return

        if self.pending_request:  # request is pending
            response_count = len(self.responses)
            print("Node " + self.code + " has responses from nodes " + str(self.responses))
            if response_count == len(list(self.connections.keys())):  # all responses received
                print("Node " + self.code + " enters critical section")
                self.clock = self.pending_request.length
                self.pending_request = None

                return

            print("Node " + self.code + " waits for responses from nodes "
                  + str(set(self.connections.keys()) - set(self.responses)))

        if self.received_requests:
            has_request = self.pending_request is not None
            print("Node " + self.code + " has requests from nodes" + str([x.node_key for x in self.received_requests]))
            handled_requests = []
            for request in self.received_requests:
                if not has_request or request.timestamp < self.pending_request.timestamp:
                    print("Node " + self.code + " sends response to node " + request.node_key)
                    self.connections[request.node_key].put_response(self.code)
                    handled_requests.append(request)

            for handled in handled_requests:  # handled requests are removed
                self.received_requests.remove(handled)





