from as02.node import Node

node_m = Node("M")
node_a = Node("A")
node_b = Node("B")
node_c = Node("C")
node_d = Node("D")


Nodes = {
    "M": node_m,
    "A": node_a,
    "B": node_b,
    "C": node_c,
    "D": node_d
}

# setup connection between nodes
for node_key in Nodes.keys():
    Nodes[node_key].add_connections(dict((code, Nodes[code]) for code in Nodes.keys() if code != node_key))

# set node B to CS
node_b.clock = 10

# send request from M
node_m.send_request(5, 1)

# send request from C
node_c.send_request(7, 4)


is_finished = False
counter = 0

while not is_finished:
    counter += 1
    print("Iteration = " + str(counter))
    is_finished = True

    for node in Nodes.values():
        node.do_job()

    for node in Nodes.values():
        if node.clock > 0 or node.received_requests or node.pending_request:
            is_finished = False

    pass