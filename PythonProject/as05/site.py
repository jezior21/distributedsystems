

class Site:
    def __init__(self, code, processes, resources):
        self.code = code
        self.processes = processes
        self.resources = resources

    def get_rst(self):
        rst = {}

        for resource in self.resources:
            rst[resource.code] = (resource.locked_by, resource.requested_by)

        return rst

    def get_pst(self):
        pst = {}

        for process in self.processes:
            pst[process.code] = (process.locked_on, process.waiting_for)

        return pst
