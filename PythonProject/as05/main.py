from as05.process import Process
from as05.resource import Resource
from as05.site import Site


def check_circle(initial_conn, connections, start, visited):

    if start[0] in visited:
        print('We have deadlock!!')
        return

    visited.add(start[0])

    neighbours = [c for c in connections if c[0] == start[1]]
    if not neighbours:
        visited.remove(start[0])
        return

    for n in neighbours:
        check_circle(initial_conn, connections, n, visited)

    visited.remove(start[0])


def process(sites):
    global_rst = {}
    global_pst = {}

    for site in sites:
        rst = site.get_rst()
        pst = site.get_pst()

        global_rst.update(rst)
        global_pst.update(pst)

    print(global_pst)
    print(global_rst)

    connections = []

    for proc_key, proc_value in global_pst.items():  # get all processes
        for locked_resource in proc_value[0]:
            resource = global_rst[locked_resource]
            waiting_processes = resource[1]  # find all processes waiting for locked resources by this process

            for waiting_proc in waiting_processes:
                # add connection between process and other waiting for its resource
                connections.append((proc_key, waiting_proc))

    connections = sorted(connections)

    print(connections)

    for conn in connections:
        start = conn
        visited = set()
        check_circle(conn, connections, start, visited)


def main():
    r1 = Resource('R1', 'P1', ['P2'])
    r2 = Resource('R2', 'P2', ['P3'])
    r3 = Resource('R3', 'P3', ['P1'])

    p1 = Process('P1', ['R1'], ['R3'])
    p2 = Process('P2', ['R2'], ['R1'])
    p3 = Process('P3', ['R3'], ['R2'])

    resources = [r1, r2, r3]
    processes = [p1, p2, p3]

    s1 = Site('S1', processes, resources)

    sites = [s1]

    process(sites)

main()



