

def test_node(node_key):
    result = set()
    result.add(node_key)
    new_added = True

    reachable = set(x for x in Nodes if Matrix[node_key][Nodes.index(x)] == 1)  # reachable from initiator in 1 hop
    result = result.union(reachable)
    while new_added:
        tmp = set()
        for n in reachable:
            xx = set(x for x in Nodes if Matrix[n][Nodes.index(x)] == 1)
            tmp = tmp.union(xx)

        reachable = tmp
        if reachable <= result:
            new_added = False
        else:
            result = result.union(reachable)

    is_good_candidate = len(result) == len(Nodes)
    print("Nodes reachable from " + node_key + " = " + str(result))
    print("Node " + node_key + " classification result = " + str(is_good_candidate))


Matrix = {
    "A": [0, 0, 1, 1, 1],
    "B": [0, 0, 0, 1, 0],
    "C": [0, 1, 0, 1, 0],
    "D": [0, 1, 0, 0, 0],
    "E": [1, 1, 0, 0, 0]
}

Nodes = list(sorted(Matrix.keys()))


# 1.1
for node in Nodes:
    test_node(node)



