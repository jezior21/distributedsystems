from as03.request import TokenResponse


class Node:
    def __init__(self, code):
        self.token = None
        self.clock = 0
        self.neighbour = None
        self.code = code
        self.pending_requests = []
        self.pending_responses = []
        self.request_sent = False
        self.request = None

    def set_connection(self, neighbour):
        self.neighbour = neighbour

    def do_job(self):
        if self.token and self.clock > 0:
            print("Node " + self.code + " has a token")
            self.clock -= 1
            return

        if self.token:
            print("Node " + self.code + " leaves CS")
            if self.pending_requests:
                self.neighbour.pending_responses.append(TokenResponse(self.pending_requests.pop(0).node_code,
                                                                   self.pending_requests, self.token))
                print("Node " + self.code + " passes a token")
                self.token = None
                self.pending_requests = []

            return

        if self.pending_responses:
            response = self.pending_responses[0]
            print("Node " + self.code + " received response for node " + str(response.node_code))
            if response.node_code == self.code:
                print("Node " + self.code + " becomes token owner")
                self.token = response.token
                self.pending_requests = response.queue
                self.clock = 5
                self.pending_responses = []
                return
            else:
                self.neighbour.pending_responses = self.pending_responses
                print("Node " + self.code + " passes response")
                self.pending_responses = []

        if self.pending_requests:
            print("Node " + self.code + " passes requests " + str(self.pending_requests))
            for request in self.pending_requests:
                self.neighbour.pending_requests.append(request)

            self.pending_requests = []

        if self.request and not self.request_sent:
            print("Node " + self.code + " sends request")
            self.request_sent = True
            self.neighbour.pending_requests.append(self.request)















