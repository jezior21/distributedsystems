from as03.node import Node
from as03.request import TokenRequest


def main():
    token = "Token"

    node_a = Node("A")
    node_b = Node("B")
    node_c = Node("C")
    node_d = Node("D")
    node_e = Node("E")
    node_f = Node("F")
    node_g = Node("G")
    node_h = Node("H")

    node_a.set_connection(node_b)
    node_b.set_connection(node_c)
    node_c.set_connection(node_d)
    node_d.set_connection(node_e)
    node_e.set_connection(node_f)
    node_f.set_connection(node_g)
    node_g.set_connection(node_h)
    node_h.set_connection(node_a)

    node_f.token = token
    node_f.clock = 5

    nodes = [node_a, node_b, node_c, node_d, node_e, node_f, node_g, node_h]

    node_a.request = TokenRequest(node_a.code)
    node_c.request = TokenRequest(node_c.code)
    node_e.request = TokenRequest(node_e.code)

    is_finished = False
    iteration = 0
    while not is_finished:
        is_finished = True
        iteration += 1
        print("Iteration = " + str(iteration))
        for node in nodes:
            node.do_job()

        for node in nodes:
            if node.clock > 0 or node.pending_requests or node.pending_responses:
                is_finished = False

        if iteration > 100:
            return

main()

