
class TokenRequest:
    def __init__(self, node_code):
        self.node_code = node_code

    def __repr__(self):
        return "<TokenRequest from node " + self.node_code + ">"


class TokenResponse:
    def __init__(self, node_code, queue, token):
        self.node_code = node_code
        self.queue = queue
        self.token = token

    def __repr__(self):
        return "<TokenResponse to node " + self.node_code + ">"

