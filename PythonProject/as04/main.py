from as04.node import Node


def main():
    nodes = {
        1: Node(1, None),
        2: Node(2, 1),
        3: Node(3, 2),
        4: Node(4, 1),
        5: Node(5, 4),
        6: Node(6, 2),
        7: Node(7, 6)
    }

    for node in nodes.values():
        node.nodes = nodes

    for node in nodes.values():
        print(node)

    nodes[6].requests.append(6)
    nodes[5].requests.append(5)

    is_finished = False
    iteration = 0
    while not is_finished:
        is_finished = True
        iteration += 1
        print("Iteration = " + str(iteration))

        for node in nodes.values():
            node.do_job()

        for node in nodes.values():
            if node.clock > 0 or node.requests:
                is_finished = False

        if iteration > 100:
            return

main()



