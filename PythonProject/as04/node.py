

class Node:
    def __init__(self, index, parent):
        self.index = index
        self.nodes = {}
        self.parent = parent
        self.token = None
        self.requests = []
        self.clock = 0
        self.sent = False

    def rotate_connection(self):
        request = self.requests.pop(0)
        child = self.nodes[request]
        self.parent = child.index

        print("Node " + str(self.index) + " rotates connection with node " + str(child.index))

        child.parent = None
        if self.requests:
            print("Node " + str(self.index) + " sends request to its parent")
            child.requests.append(self.index)
            self.sent = True
        else:
            self.sent = False

    def do_job(self):
        if self.requests:
            print("Node " + str(self.index) + " has requests from " + str(self.requests))

        if self.clock > 0:
            print("Node " + str(self.index) + " is root and is in CS")
            self.clock -= 1
            return

        if self.parent is None:
            print("Node " + str(self.index) + " is root and is not in CS")
            if self.requests:
                request = self.requests[0]
                if request == self.index:
                    self.requests.pop(0)
                    self.clock = 3
                else:
                    self.rotate_connection()
                return

        if self.requests and self.parent and not self.sent:
            print("Node " + str(self.index) + " sends request to its parent")
            self.nodes[self.parent].requests.append(self.index)
            self.sent = True

    def __repr__(self):
        return "Node " + str(self.index) + " parent = " + str(self.parent)

